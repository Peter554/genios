const puppeteer = require("puppeteer");
const datefns = require("date-fns");
const jsonfile = require("jsonfile");
const sade = require("sade");

const formatDateGenios = (date) => datefns.format(date, "dd.MM.yyyy");
const formatDateResults = (date) => datefns.format(date, "yyyy-MM-dd");

const getIntervals = (from, to, next) => {
  let date = from;
  const o = [];
  while (datefns.isBefore(date, to)) {
    o.push({
      from: date,
      to: next(date),
    });
    date = next(date);
  }
  return o;
};

const getResults = (query, from, to, daily) => {
  const fileName = `results_query=${query.replace(
    " ",
    "-"
  )}_from=${formatDateResults(from)}_to=${formatDateResults(to)}_${
    daily ? "daily" : "monthly"
  }.json`;

  jsonfile.writeFileSync(fileName, {});

  return {
    add: (date, record) => {
      const o = jsonfile.readFileSync(fileName);
      jsonfile.writeFileSync(fileName, {
        ...o,
        [formatDateResults(date)]: record,
      });
    },
  };
};

const search = async (query, from, to) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  await page.goto("https://www.genios.de/");

  await (await page.$("#field_q")).type(query);

  await (await page.$("#extendedFormTrigger")).click();

  await (
    await page.waitForSelector("#dateFieldSelector", { visible: true })
  ).click();

  await (await page.$("#field_DT_from")).type(formatDateGenios(from));
  await page.keyboard.press("Tab");
  await page.keyboard.press("Backspace");
  await (await page.$("#field_DT_to")).type(formatDateGenios(to));

  await (await page.$("#searchButtonBottom")).click();

  await page.waitForSelector(".moduleResultTextHeader", { visible: true });

  const html = await page.$eval(
    ".moduleResultTextHeader",
    (node) => node.innerHTML
  );

  await page.close();
  await browser.close();

  const match = html.match(/(?<count>[0-9.]+)/);
  const { count } = match.groups;
  return parseInt(count.replace(".", ""));
};

const run = async (query, from, to, options) => {
  const { daily } = options;

  const results = getResults(query, from, to, daily);

  const intervals = daily
    ? getIntervals(from, to, (date) => datefns.addDays(date, 1))
    : getIntervals(from, to, (date) => datefns.addMonths(date, 1));

  for (let i = 0; i < intervals.length; i++) {
    console.log(`[${i + 1}/${intervals.length}]`);

    const { from, to } = intervals[i];

    const record = { count: -1, error: undefined };
    try {
      record.count = await search(query, from, to);
    } catch (err) {
      record.error = `[${err.name}] ${err.message}`;
    }

    results.add(from, record);
  }

  console.log("Done!");
};

sade("genios <query> <from> <to>", true)
  .option("--daily", "Collect daily data", false)
  .action((query, from, to, options) =>
    run(query, new Date(from), new Date(to), options)
  )
  .example("bamboo 2000-01-01 2010-01-01")
  .parse(process.argv);
